<?php
/**
 * form.php
 * Created By
 * feri_
 * 23/01/2023
 */
use yii\helpers\Url;
use yii\bootstrap5\ActiveForm;
$csrf=Yii::$app->request->csrfParam;
$token= Yii::$app->request->getCsrfToken();
$readonly=true;
?>
<?php $form = ActiveForm::begin([
    'id' => 'user-form',
//        'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{input}\n{error}",
        'labelOptions' => ['class' => 'form-label'],
        'inputOptions' => ['class' => 'form-control'],
//        'errorOptions' => ['class' => 'col-lg-7 invalid-feedback'],
    ],
    'options' => [
        'class'=>'row g-3',

    ]
]); ?>
<div class="col-12">
    <?= $form->field($model,'username')->textInput(['maxlength'=>true]) ?>
</div>
<div class="col-12">
   <?= $form->field($model,'password_hash')->passwordInput() ?>
</div>
<div class="col-12">
    <?= $form->field($model,'level')->dropDownList($model->listLevel)?>
</div>

<div class="col-12">
    <button class="btn btn-primary" type="submit">Submit</button>
</div>
<?php ActiveForm::end(); ?>
<script>
    $(function(){
        "use strict";
        $('.select2').select2({
            theme: 'bootstrap4',
             width:  '100%',
            placeholder: $(this).data('placeholder'),
            allowClear: Boolean($(this).data('allow-clear')),
            ajax: {
                url: function (a) {
                    return this.attr('data-url');
                },
                data: function (params) {
                    let el = this;
                    let query = {
                        q: params.term,
                        // param: {
                        //     // col: el.attr('data-depend'),
                        //     // fromcol: el.attr('data-depend-from'),
                        //     filter: form.find('#' + el.attr('data-depend-id')).val(),
                        // }
                    };
                    return query;
                },
                processResults: function (data) {
                    return {
                        results: data.results
                    };
                }
            }
        })
        $('.select2').on('select2:select',function (e){
            let data=e.params.data
            let elm=$(this);
            if(data.id==0){

               // console.log(elm.attr('data-name'))
                $.ajax({
                    url:elm.attr('data-action'),
                    method:'POST',
                    data:{description:data.text},
                }).then(function (data) {
                    console.log(data)
                    // create the option and append to Select2
                    let option = new Option(data.text, data.id, true, true);
                    elm.append(option).trigger('change')
                    // manually trigger the `select2:select` event
                    elm.trigger({
                        type: 'select2:select',
                        params: {
                            data: data
                        }
                    })
                });
            }

        })
    })
</script>
