<?php
/**
 * index.php
 * Created By
 * feri_
 * 27/01/2023
 */
use app\synui\ThemeAssets;
use yii\helpers\Url;
use yii\bootstrap5\Html;
$assets=ThemeAssets::register($this);
$ajax=Url::to();
?>
<link href="<?= $assets->baseUrl ?>/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
<link href="<?= $assets->baseUrl ?>/plugins/sweetalert2/css/sweetalert2.min.css" rel="stylesheet" />

<script src="<?= $assets->baseUrl ?>/plugins/datatable/js/jquery.dataTables.min.js"></script>
<script src="<?= $assets->baseUrl ?>/plugins/datatable/js/dataTables.bootstrap5.min.js"></script>
<script src="<?= $assets->baseUrl ?>/plugins/sweetalert2/js/sweetalert2.all.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.3.200/pdf.js"></script>
<!--start breadcrumb-->
<div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
    <div class="breadcrumb-title pe-3">Users</div>
    <div class="ps-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb mb-0 p-0 align-items-center">
                <li class="breadcrumb-item"><a href="javascript:;"><ion-icon name="person-circle-sharp"></ion-icon></a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">List User</li>
            </ol>
        </nav>
    </div>
    <div class="ms-auto">
        <div class="btn-group">
            <?=Html::tag('a','New',['href'=>Url::to(['/user/default/create']),'class'=>'btn btn-outline-primary']) ?>
        </div>
    </div>
</div>
<!--end breadcrumb-->
<h6 class="mb-0 text-uppercase">List Document</h6>
<hr/>
<div class="card radius-10">
    <div class="card-body">
        <div class="table-responsive">
            <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th>Action</th>
                    <th>User Name</th>
                    <th>Level</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<script>
    $(function(){
        let mv=$('#mdl');
        let groupColumn = 1;
        let table=$('#example').DataTable({
            "orderCellsTop": true,
            "scrollX": true,
            "scrollY": "400px",
            "processing": true,
            "serverSide": true,
            "ajax":{
                "url":"<?php $ajax ?>",
                "data":function(d){
                    d.detail=true;
                    return d
                }
            },
            "columns":[
                {"data":"actions","orderable":false,"width":"10%"},
                {"data":"username","orderable":true},
                {"data":"status","orderable":true,"width":"15%"}
            ],
            "order": [[ groupColumn, 'asc' ]],
            "displayLength": 25,
            // "drawCallback": function ( settings ) {
            //     var api = this.api();
            //     var rows = api.rows( {page:'current'} ).nodes();
            //     var last=null;
            //     api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
            //         if ( last !== group ) {
            //             $(rows).eq( i ).before(
            //                 '<tr class="group"><td colspan="4">'+group+'</td></tr>'
            //             );
            //             last = group;
            //         }
            //     } );
            // },
        });
        $('body').delegate('[data-action=view]','click',function (e){
            e.preventDefault();
            mv.modal('show');
            $.ajax({
                method:'GET',
                url:$(this).attr('href'),
                beforeSend:function (){
                    mv.find('.modal-body').html('<p> Please  wait</p>');
                },
                success:function (res){
                    mv.find('.modal-body').html(res);
                },
                error:function (res){
                    mv.hide();
                }
            })
        })
        $('body').delegate('[data-action=delete]','click',function (e){
            e.preventDefault();
            let elm=$(this);
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        method:'POST',
                        url:elm.attr('href'),
                        success:function (res){
                            Swal.fire(
                                'Deleted!',
                                'Your file has been deleted.',
                                'success'
                            )
                            table.ajax.reload();
                        },
                        error:function (res,xr){
                            Swal.fire(
                                'Not Deleted!',
                                'Your file.',
                                'error'
                            )
                        }
                    })
                }
            })
        })
    })

</script>
