<?php
/**
 * create.php
 * Created By
 * feri_
 * 23/01/2023
 */
?>
<div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
    <div class="breadcrumb-title pe-3">User</div>
    <div class="ps-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb mb-0 p-0 align-items-center">
                <li class="breadcrumb-item"><a href="javascript:;"><ion-icon name="person-circle-sharp"></ion-icon></a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">New User</li>
            </ol>
        </nav>
    </div>
</div>
<div class="row">
    <div class="col-xl-8 mx-auto">
        <h6 class="mb-0 text-uppercase">Document</h6>
        <hr/>
        <div class="card">
            <div class="card-body">
                <div class="p-3 border rounded">
                    <?= $this->render('form',['model'=>$model]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
