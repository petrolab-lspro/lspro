<?php
/**
 * DefaultController.php
 * Created By
 * feri_
 * 27/01/2023
 */

namespace app\modules\user\controllers;
use app\modules\user\models\User;
use Yii;
use yii\helpers\Url;
class DefaultController extends \yii\web\Controller
{
    public function actionIndex(){
        $model=new User();
        $param=Yii::$app->request->queryParams;
        if(Yii::$app->request->isAjax){
            return $model->renderData($param);
        }
        return $this->render('index',['model'=>$model]);
    }
    public function actionCreate(){
        $model=new User();
        $post=Yii::$app->request->post();
        if($model->load($post)){
            if($model->save()){
                return $this->redirect(Url::to(['/user/default']));
            }
        }
        return $this->render('create',['model'=>$model]);
    }
    public function actionEdit($id){
        $model= User::findOne(['id_user'=>$id]);
        $post=Yii::$app->request->post();
        if($model->load($post)){
            if($model->save()){
                return $this->redirect(Url::to(['/user/default']));
            }
        }
        return $this->render('edit',['model'=>$model]);
    }

}