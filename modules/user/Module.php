<?php
/**
 * Module.php
 * Created By
 * feri_
 * 27/01/2023
 */

namespace app\modules\user;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
class Module extends \yii\base\Module
{
    public $controllerNamespace='app\modules\user\controllers';
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
}