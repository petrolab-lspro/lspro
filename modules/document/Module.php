<?php
/**
 * Module.php
 * Created By
 * feri_
 * 23/01/2023
 */
namespace app\modules\document;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
class Module extends \yii\base\Module
{
    public $controllerNamespace='app\modules\document\controllers';

    /**
     * @return array[]
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
}

