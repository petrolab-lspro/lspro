<?php
/**
 * FormCategory.php
 * Created By
 * feri_
 * 03/02/2023
 */

namespace app\modules\document\models;

use ArrayAccess;
use Yii;
use yii\bootstrap5\Html;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
class FormCategory extends Categories
{
    public $fileUpload;
    public $_cat_id;
    public function rules()
    {
        return array_merge(parent::rules(),[
            [['cat_id'],'safe'],
            [['fileUpload'], 'file', 'extensions' => 'pdf,docx,doc,xlsx,xls', 'maxFiles' => 5],
        ]);
    }



    public function load($data, $formName = null)
    {
        if(parent::load($data, $formName)){
            return true;
        }
        return false;
        // TODO: Change the autogenerated stub
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        if(parent::save($runValidation, $attributeNames)){
            $files=UploadedFile::getInstances($this,'fileUpload');
            if($files!=[]){
              foreach ($files as $file){
                $fn=$file->baseName.'.'.$file->extension;
                  $model=Documents::findOne(['file_name'=>$fn,'category_id'=>$this->id]);
                  if($model==null){
                      $model=new Documents(['category_id'=>$this->id]);
                  }
                  $model->name=$file->baseName;
                  $model->file_name = $file->baseName . '.' . $file->extension;
                  $model->level=$this->level;
                  $model->user_create = Yii::$app->user->identity->username;
                  if($model->save()){
                      $file->saveAs($model->dirBerkas . '/' . $model->file_name);
                  }
              }
            }
            return true;
        }
        return false;
        // TODO: Change the autogenerated stub
    }

    public function delete(){
        if(parent::delete()){
            return true;
        }
        return false;
    }
    public function getCat_id(){
        if($this->_cat_id==null){
            $this->_cat_id=$this->id;
        }
        return $this->_cat_id;
    }

    public function setCat_id($new){
        $this->_cat_id=$new;
    }


   
}