<?php
/**
 * FormDoct.php
 * Created By
 * feri_
 * 08/02/2023
 */

namespace app\modules\document\models;

use yii\web\UploadedFile;
use Yii;
class FormDoct extends \yii\base\Model
{
    public $fileUpload;
    public $_category_id;
    public $_description;
    public $_level;
    public $_type;


    public function rules()
    {
        return [
            [['description'], 'safe'],
            [['type','level','category_id'], 'integer'],
            [['description'], 'string', 'max' => 200],
            [['fileUpload'], 'file', 'extensions' => 'pdf,png,jpeg,jpg', 'maxFiles' => 5,],
        ];
    }

    /**
     * @return mixed
     */
    public function getCategory_id(){
        return $this->_category_id;
    }

    public function setCategory_id($id){
        $this->_category_id=$id;
    }

    public function getDescription(){
        return $this->_description;
    }

    public function setDescription($new){
        $this->_description=$new;
    }

    public function getType(){
        return $this->_type;
    }

    public function setType($new){
        $this->_type=$new;
    }

    public function getLevel(){
        return $this->_level;
    }

    public function setLevel($new){
        $this->_level=$new;
    }


    public static function findOne($id){
        $category=Categories::findOne(['id'=>$id]);
        $model=new static();
        $model->_category_id=$category->id;
        $model->_level=$category->level;
        $model->_description=$category->description;
        return $model;
    }

    public function save(){
        $out=false;
//        $category=Categories::findOne(['id'=>$this->_category_id]);
//        if($category==null){
//            $category=New Categories();
//        }
//        $category->description=$this->description;
//        $category->level=$this->level;
//        $category->type=$this->type;
//        $out=$category->save();
        if($this->category_id==0){
            $category=new Categories();
            $category->description=$this->description;
            $category->level=$this->level;
            $category->type=$this->type;
            $out=$category->save();
            $this->_category_id=$category->id;
        }
        $files=UploadedFile::getInstances($this,'fileUpload');
        if($files!=[]){
            foreach ($files as $file){
//                $model=Documents::findOne(['name'=>$file->baseName,'category_id'=>$this->id]);
//                if($model==null){
                    $model=new Documents(['category_id'=>$this->category_id]);
//                }
                $model->name=$file->baseName;
                $model->file_name = $file->baseName . '.' . $file->extension;
                $model->level=$this->level;
                $model->user_create = Yii::$app->user->identity->username;
                if($model->save()){
                    $file->saveAs($model->dirBerkas . '/' . $model->file_name);
                    $out=true;
                }else{
                    $out=false;
                }
            }
        }
        return $out;
    }
}