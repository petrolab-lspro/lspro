<?php
/**
 * Categories.php
 * Created By
 * feri_
 * 23/01/2023
 */

namespace app\modules\document\models;

use app\component\DataTableAjax;
use yii\bootstrap5\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use app\modelsDB\Users;
use yii\helpers\ArrayHelper;
use Yii;

/**
 *
 * @property-read void $userCreate
 * @property-read string $actions
 * @property-read string $categoryName
 * @property-read string $doc
 * @property-read \yii\db\ActiveQuery $document
 * @property-read void $nama
 */
class Categories extends \app\modelsDB\TblCategoryDocument
{

    public function rules()
    {
        return [
            [['description'], 'required'],
            [['type','level'], 'integer'],
            [['description'], 'string', 'max' => 200],
            [['user_id'], 'string', 'max' => 100],
            [['userArray'],'safe'],
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     *
     */
    public function getDocument(){
        return $this->hasMany(Documents::className(),['category_id'=>'id']);
    }

    /**
     * @return string
     */
    public function getActions(){

        if(Yii::$app->user->identity->level==2){
            $out=Html::tag('div',
            Html::tag('div',
                Html::tag('a','<i class="bx bx-pen"></i>',['href'=>Url::to(['/document/default/edit','id'=>$this->id,'jenis'=>$this->getReq($this->type)]),'class'=>'btn btn-sm btn-outline-primary','title'=>'edit jenis dokumen',]).
                Html::tag('a','<i class="bx bx-trash-alt"></i>',['href'=>Url::toRoute(['/document/default/delete','id'=>$this->id,'title'=>'delete jenis dokumen']),'class'=>'btn btn-sm btn-outline-danger','data-action'=>'delete']).
                Html::tag('a','<i class="bx bx-show"></i>',['href'=>Url::toRoute(['/document/default/view','id'=>$this->id,'jenis'=>$this->getReq($this->type)]),'class'=>'btn btn-sm btn-outline-secondary','title'=>'view file dokumen'])
                ,['class'=>'btn-group']),
            ['class'=>'col']);
            return $out;
        }
        return  Html::tag('div',
            Html::tag('div',
             Html::tag('a','<i class="bx bx-show"></i>',['href'=>Url::toRoute(['/document/default/view','id'=>$this->id,'jenis'=>$this->getReq($this->type)]),'class'=>'btn btn-sm btn-outline-secondary','title'=>'view file dokumen'])
                ,['class'=>'btn-group']),
            ['class'=>'col']);
    }

    public function getDetail(){
        return '';
    }
    public function renderData($params){
        $q=self::find();
        $type=1;
        if(isset($params['jenis'])&& $params['jenis']!=''){
            if($params['jenis']=='ISO 17065'){
                $type=1;
            }
            if($params['jenis']=='internal'){
                $type=3;
            }
            if($params['jenis']=='eksternal'){
                $type=2;
            }
        }
        if(isset($params['jenis'])){
            $q->andFilterWhere(['type'=>$type]);
        }
        if(Yii::$app->user->identity->level!=2){
            $q->andFilterWhere(['like','user_id',Yii::$app->user->identity->id_user]);
        }
        $total=$q->count('id');
        if (isset($params['columns'])){
            foreach ($params['columns'] as $col) {
                if (isset($params['search']['value']) && $params['search']['value'] != '') {
                    if($this->hasAttribute($col['data'])){
                        $q->orFilterWhere(['like',  $col['data'], $params['search']['value']]);
                    }
                    if($col['data']=='name'){
//                        $q->andFilterWhere(['like',  'tbl_document.name', $params['search']['value']]);
                           $q->andWhere("tbl_document.name like '%".$params['search']['value']."%'");
                    }
                    if($col['data']=='doc'){
                        $d=Documents::find()->where(['like','name',$params['search']['value']])->select(['category_id'])->all();
                        $q->andWhere(' `' . self::tableName() . "`.id IN ('" . join("','", ArrayHelper::getColumn($d, 'category_id')) . "') ");
//                        $q->andFilterWhere(['like',  'description', $col['search']['value']]);
                    }
                }
            }
        }

        if (isset($params['order'])) {
            $sort = [];
            foreach ($params['order'] as $item){
                $arr = isset($item['dir']) ? $item['dir'] : 'asc';
                $col = isset($item['column']) && isset($params['columns'][$item['column']]) ? $params['columns'][$item['column']]['data'] : '0';
//                if ((new self())->hasAttribute($col)) {
//                    $sort = self::tableName() . '.' . $col . ' ' . $arr;
//                }
                if ($col == 'name') {
                    $sort =  'tbl_document.name ' . $arr;
                }
                if($col=='dateCreate'){
                    $sort='tbl_document.create_at '. $arr;
                }
            }
            $q->orderBy($sort);
        }
    
       return DataTableAjax::renderAjax($q, ['allData' => $total]);
    }

    /**
     * @return void
     */
    public function getNama(){
        $out='';
        if($this->tblDocuments==[]){
            return '';
        }
        foreach ($this->tblDocuments as $doc){
            $out.=$doc->name.'<br>';
        }
        return $out;
    }

    /**
     * @return void
     */
    public function getUserCreate(){
        $out='';
        if($this->tblDocuments==[]){
            return '';
        }
        foreach ($this->tblDocuments as $doc){
            $out.=$doc->user_create.'<br>';
        }
        return $out;
    }

    /**
     * @return string
     */
    public function getCategoryName(){
        return $this->description;
    }
    public function getDoc(){
           $out=Html::tag('thead',Html::tag('tr',
                Html::tag('td','').
                Html::tag('td','Dibuat',['width'=>'15%']).
                Html::tag('td','Tanggal',['width'=>'15%'])
            )
        ).'<tbody>';
        if($this->tblDocuments==[]){
           return '';
        }
        foreach ($this->document as $doc){
            $out.=Html::tag('tr',
                Html::tag('td',Html::tag('a',
                    Html::tag('h6', $doc->name,['class'=>'product-name mb-2']),['href'=>Url::to(['/document/default/view-pdf','id'=>$doc->id]),'data-action'=>'view'])).
                Html::tag('td',$doc->user_create).
                Html::tag('td',date('d-m-Y',strtotime($doc->create_at)))
            );
        }
        return
            Html::tag('table',$out.'</tbody>',['class'=>'table','width'=>'100%']);
    }

    public $_userArray;
    public function getUserArray(){
        if($this->_userArray==null){
            $this->_userArray=explode(',',$this->user_id);
        }
        return $this->_userArray;
    }

    public function setUserArray($arr){
        if(is_array($arr)){
            $this->user_id=join(',',$arr);
        }
        $this->_userArray=$arr;
    }

    public function getListUser(){
        return ArrayHelper::map(Users::find()->all(),'id_user','username');
    }
  

   
    public function renderJenis($new){
        if($new=='ISO 17065'){
            $this->type=1;
        }
        if($new=='internal'){
            $this->type=3;
        }
        if($new=='eksternal'){
            $this->type=2;
        }
    }

    public function getReq($type){
        if($type==1){
            return 'ISO 17065';
        }
        if($type==2){
            return 'eksternal';
        }
        return 'internal';
    }
}