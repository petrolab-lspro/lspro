<?php
/**
 * Documents.php
 * Created By
 * feri_
 * 23/01/2023
 */

namespace app\modules\document\models;
use app\component\DataTableAjax;
use yii\helpers\Url;
use yii\bootstrap5\Html;
use Yii;
/**
 *
 * @property-read string $dirBerkas
 * @property-read string $categoryName
 * @property-read string $urlFile
 */
class Documents extends \app\modelsDB\TblDocument
{

    public $oldName;
    /**
     * @return string
     */
    public function getActions(){
        $type=$this->type==1?'internal':'eksternal';
        $btnDelete= Yii::$app->user->identity->level==2?Html::tag('a','<i class="bx bx-trash-alt"></i>',['href'=>Url::toRoute(['/document/default/delete-document','id'=>$this->id]),'class'=>'btn btn-xs btn-outline-danger','data-action'=>'delete','title'=>'delete']):'';
        $btnEdit=Yii::$app->user->identity->level==2?Html::tag('a','<i class="bx bx-pen"></i>',['href'=>Url::toRoute(['/document/'.$type.'/edit','id'=>$this->id]),'class'=>'btn btn-xs btn-outline-primary','title'=>'edit','data-action'=>'edit']):'';
        return Html::tag('div',
            Html::tag('div',
                // $btnEdit.
                    $btnDelete.
                Html::tag('a','<i class="bx bx-show"></i>',['href'=>Url::toRoute(['/document/default/view-pdf','id'=>$this->id]), 'class'=>'btn btn-sm btn-outline-primary','data-action'=>'view','title'=>'view'])
                ,['class'=>'btn-group']),
            ['class'=>'col']);
//        <div class="col">
//                      <div class="btn-group">
//                        <button type="button" class="btn btn-primary">Button</button>
//                        <button type="button" class="btn btn-primary">Button</button>
//                        <button type="button" class="btn btn-primary">Button</button>
//                      </div>
//                    </div>
    }

    /**
     * @return false|string
     */
    public function getDateCreate(){
        return $this->create_at==null?'':date('d-m-Y h:s:i',strtotime($this->create_at));
    }
    /**
     * @param $params
     * @return mixed
     */

    public function renderData($params){
        $q=self::find();
        if(isset($params['type'])){
            $q->where(['type'=>$params['type']]);
        }
        // if(Yii::$app->user->identity->level!=2){
        //     $q->andFilterWhere(['like','user_id',Yii::$app->user->identity->id_user]);
        // }
        if(isset($params['category_id'])){
           $q->andFilterWhere(['=','category_id',$params['category_id']]); 
        }
        $total=$q->count('id');
        return DataTableAjax::renderAjax($q, ['allData' => $total]);
    }

    /**
     * @return string
     */
    public function getCategoryName(){
        return $this->category==null?'':$this->category->description;
    }
    /**
     * @return string
     */
    public function getDirBerkas()
    {
        $dir = Yii::getAlias('@app');
        $folder = '/document';
        if (!file_exists($dir . $folder) && !mkdir($concurrentDirectory = $dir . $folder, 0777, true) && !is_dir($concurrentDirectory)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
        }
        return $dir . $folder;
    }

    /**
     * @return string
     */
    public function getUrlFile()
    {
        if($this->file_name != null){
            Yii::$app->assetManager->publish($this->dirBerkas);
            return Yii::$app->assetManager->getPublishedUrl($this->dirBerkas) . "/" . $this->file_name;
//              return $this->dirBerkas .'/'.$this->file_name;
        }
        return "";
    }

    public function deleteFile(){
        $path=$this->dirBerkas.'/'.$this->file_name;
        if(file_exists($this->dirBerkas.'/'.$this->file_name)) {
            if (unlink($path)) {
                return true;
            }
            return false;
        }
        return true;
    }

    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert)){
            if($this->isNewRecord){
                $this->create_at=date('Y-m-d h:i:s');
            }else{
                $this->update_at=date('Y-m-d h:i:s');
            }
            return true;
            // TODO: Change the autogenerated stub
        }
        return false;
    }

    /**
     * @param $id
     * @return string
     */
    public function renderDetails($id){
        $q=self::find()->where(['category_id'=>$id])->orderBy('name');
        $out=Html::tag('thead',
            Html::tag('tr',
            Html::tag('td','Option',['style'=>'15%']).
            Html::tag('td','No',['style'=>'width:5%']).
            Html::tag('td','Nama Document').
            Html::tag('td','Revisi',['style'=>'width:10%']).
            Html::tag('td','Tahun Terbit',['style'=>'width:10%']).
            Html::tag('td','Dibuat',['style'=>'width:15%']).
            Html::tag('td','Tgl Upload',['style'=>'10%']))).'<tbody>';
            foreach ($q->all() as $idx=>$item){
                $out.=Html::tag('tr',
                Html::tag('td',$item->actions).
                    Html::tag('td',$idx+1).
                    Html::tag('td',$item->name).
                    Html::tag('td',$item->rev).
                    Html::tag('td',$item->tahun).
                    Html::tag('td',$item->user_create).
                    Html::tag('td',$item->create_at)
                    ,['data-id'=>$item->id]);
            }
            return Html::tag('table',$out.'</tbody>',['class'=>'table table-responsive','id'=>'tbl-doc']);
    }

    public function getTahunText(){
        if(Yii::$app->user->identity->level==2){
            return Html::textInput('tahun',$this->tahun,['class'=>'form-control txt','data-url'=>Url::to(['/document/default/edit-document','id'=>$this->id])]);
        }
        return $this->tahun;
    }

    public function getLevelText(){
        if(Yii::$app->user->identity->level==2){
            return Html::textInput('level',$this->level,['class'=>'form-control txt','data-url'=>Url::to(['/document/default/edit-document','id'=>$this->id])]);
        }
        return $this->level;
    }
    public function getRevText(){
        if(Yii::$app->user->identity->level==2){
            return Html::textInput('rev',$this->rev,['class'=>'form-control txt','data-url'=>Url::to(['/document/default/edit-document','id'=>$this->id])]);
        }
        return $this->rev;
    }
}
