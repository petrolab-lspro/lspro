<?php
/**
 * EksternalController.php
 * Created By
 * feri_
 * 03/02/2023
 */

namespace app\modules\document\controllers;
use app\modules\document\models\FormCategory;
use app\modules\document\models\FormDoct;
use app\modules\document\models\FormDocument;
use yii\web\Controller;
use Yii;
use yii\helpers\Url;
use yii\web\Response;
class EksternalController extends Controller
{
    public function behaviors()
    {
        return [
               'access' => [
                   'class' => \yii\filters\AccessControl::className(),
                   'rules' => [
                       [
                           'actions' => ['new','edit','delete'],
                           'allow' =>Yii::$app->user->identity->level==2,
                           'roles' => ['@'],
                       ],
                       [
                        'actions' => ['index','details'],
                           'allow' =>true,
                           'roles' => ['@'],
                       ]

                   ],
               ]
           ];
    }
    public function actionIndex(){

        $model=new FormDocument();
        $param=Yii::$app->request->queryParams;
        if(Yii::$app->request->isAjax){
            return $model->renderData($param);
        }
        return $this->render('index',['model'=>$model]);
    }

    public function actionNew(){
        $model=new FormDocument();
        $model->type=2;
        $post=Yii::$app->request->post();
        if($model->load($post)){
            if($model->save()){
                return $this->redirect(Url::to(['/document/eksternal']));
            }
        }
        return $this->render('create',['model'=>$model]);
    }
    public function actionEdit($id){
        $model=FormDocument::findOne(['id'=>$id]);
        $post=Yii::$app->request->post();
        if($model->load($post)){
            if($model->save()){
                return $this->redirect(Url::to(['/document/eksternal']));
            }else{
                print_r($model->errors);
                exit;
            }
        }
        return $this->render('edit',['model'=>$model]);
    }
    
    public function actionDetails(){
        $id=Yii::$app->request->get(['id']);
        $model=new FormDocument();
        Yii::$app->response->format=Response::FORMAT_JSON;
        return $model->renderDetails($id);
    }

    public function actionEditJenis($id){
        $model=FormCategory::find()->where(['id'=>$id])->one();
        return $this->renderAjax('edit_jenis',['model'=>$model]);
    }
}