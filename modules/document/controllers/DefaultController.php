<?php
/**
 * DefaultController.php
 * Created By
 * feri_
 * 23/01/2023
 */

namespace app\modules\document\controllers;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use app\modules\document\models\FormDocument;
use app\modules\document\models\Categories;
use app\modules\document\models\FormCategory;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
class DefaultController extends \yii\web\Controller
{
    public function beforeAction($action)
    {
        if (in_array($action->id, ['create-category','delete','delete-document'])) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionListCategory(){
        $g=isset($_GET['q'])?$_GET['q']:'';
        $type=$_GET['type'];
        $q=Categories::find()->where("type='$type'");
        $q->andWhere(['like','description',$g]);
        $q->select(['id','description','level']);
        $out=[];
        foreach ($q->all() as $rec){
            $out[]=[
                'id'=>$rec->id,
                'text'=>$rec->description,
                'level'=>$rec->level,
            ];
        }
        if($q->all()==null){
            $out[]=[
                'id'=>0,
                'text'=>$g,
                'level'=>0
            ];
        }
        Yii::$app->response->format=Response::FORMAT_JSON;
        return [
            'results'=>$out
        ];
    }
    public function actionIndex(){
        $model=new FormCategory();
        $param=Yii::$app->request->queryParams;
        // $param['jenis']=$param['jenis'];
        if(Yii::$app->request->isAjax){
            return $model->renderData($param);
        }
        return $this->render('_index',['model'=>$model]);
    }

    public function actionNew(){
        $request=Yii::$app->request->queryParams;
        $model=new FormCategory();
        $model->renderJenis($request['jenis']);
        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['/document/default','jenis'=>$request['jenis']]);
        }
        // $model->level=$_GET['level'];
        // if(Yii::$app->request->isAjax){
        //     Yii::$app->response->format=Response::FORMAT_JSON;
        //     if($model->load(Yii::$app->request->post())){
        //        return [
        //            'status'=>$model->save()?'success':'error'
        //        ];
        //     }
        //     return $this->renderAjax('form',['model'=>$model]);
        // }
        return $this->render('create',['model'=>$model]);
    }

    public function actionEdit($id){
        $model=$this->findModel($id);
        $request=Yii::$app->request->queryParams;
        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['/document/default','jenis'=>$request['jenis']]);
        }
        return $this->render('edit',['model'=>$model]);
    }

    public function actionDelete($id){
        $model=$this->findModel($id);
        Yii::$app->response->format=Response::FORMAT_JSON;
        return [
            'status'=>$model->delete()?'success':'error',
            'data'=>array_merge($model->toArray(),[
                'actions'=>$model->actions,
            ])
            ];
    }
    public function actionDeleteDocument($id){
        $model=FormDocument::find()->where(['id'=>$id])->one();
        Yii::$app->response->format=Response::FORMAT_JSON;
        return [
            'status'=>$model->delete()?'success':'error',
            'data'=>array_merge($model->toArray(),[
                'actions'=>$model->actions,
            ])
            ];
    }
    public function actionView($id){
        $model=new FormDocument();
        $request=Yii::$app->request->queryParams;
        $request['category_id']=$id;
        if(Yii::$app->request->isAjax){
           return $model->renderData($request);
        }
        return $this->render('view',['model'=>$model]);
    }

    public function actionViewPdf($id){
        $userLevel=Yii::$app->user->identity->level;
        $model=FormDocument::find()->where(['id'=>$id])->one();
        $level=$model->level;
        $img=getimagesize($model->getDirBerkas().'/'.$model->file_name);
        Yii::$app->response->format=Response::FORMAT_JSON;
        if($userLevel==0){
            if($level==0 || $level==4){
                return [
                    'form'=>$this->renderAjax('img',['model'=>$model]),
                    'file'=>$model->getUrlFile()
                ];
            }
            return [
                'form'=>$this->renderAjax($img==null?'pdf1':'img',['model'=>$model]),
                'file'=>$model->getUrlFile()
            ];
        }
        return [
            'form'=>$this->renderAjax('img',['model'=>$model]),
            'file'=>$model->getUrlFile()
        ];
//        return $this->renderAjax('pdf1',['model'=>$model]);
    }

    public function actionCreateCategory(){
        $model=new Categories();
        $post=Yii::$app->request->post();
        if(isset($post['description']) && $post['description']!=''){
            $model->description=$post['description'];
            Yii::$app->response->format=Response::FORMAT_JSON;
            if($model->save()){
                return [
                    'id'=>$model->id,
                    'text'=>$model->description
                ];
            }
            return [
                'id'=>0,
                'text'=>'text'
            ];
        }
    }
    public function actionDetails(){
        $id=$_GET['id'];
        $model=new FormDocument();
        Yii::$app->response->format=Response::FORMAT_JSON;
        return $model->renderDetails($id);
    }

    protected function findModel($id)
    {
        if (($model = FormCategory::findOne(['id' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionEditDocument($id){
        $post=Yii::$app->request->post();
        $model=FormDocument::find()->where(['id'=>$id])->one();
        // if($model!=null){
            if(isset($post['tahun'])){
                $model->tahun=$post['tahun'];
            }
            if(isset($post['rev'])){
                $model->rev=$post['rev'];
            }
            if(isset($post['level'])){
                $model->level=$post['level'];
            }
            Yii::$app->response->format=Response::FORMAT_JSON;
             return [
                'status'=>$model->save()?'success':'error',
                'data'=>array_merge($model->toArray(),[
                    'tahunText'=>$model->tahunText,
                    'actions'=>$model->actions,
                    'levelText'=>$model->levelText,
                    'revText'=>$model->revText,
                ])
            ];   
           
        // }
    }

    public function actionEditJenis($id){
        $model=Categories::find()->where(['id'=>$id])->one();
        if($model->load(Yii::$app->request->post())){
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format=Response::FORMAT_JSON;
                return [
                    'status'=>$model->save()?'success':'error',
                    'message'=>'save',
                    'data'=>array_merge($model->toArray(),
                    [
                        'detail'=>$model->detail,
                        'actions'=>$model->actions
                    ])
                ]; 
            }
            if($model->save()){
                if($model->type==1){
                    return $this->redirect(Url::to(['/document/internal']));
                }
                return $this->redirect(Url::to(['/document/eksternal']));
            }
            
        }
        return $this->render('edit_jenis',['model'=>$model]);
    }
}