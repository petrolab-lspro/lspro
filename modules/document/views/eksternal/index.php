<?php
/**
 * index.php
 * Created By
 * feri_
 * 23/01/2023
 */
use app\synui\ThemeAssets;
use yii\helpers\Url;
use yii\bootstrap5\Html;
$assets=ThemeAssets::register($this);
$ajax=Url::to();
$urlDetails=Url::to(['/document/default/details']);
$a=Yii::getAlias('@web');
$a=str_replace('web','',$a);
$csrf=Yii::$app->request->csrfToken;
?>

<link href="<?= $assets->baseUrl ?>/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
<link href="<?= $assets->baseUrl ?>/plugins/sweetalert2/css/sweetalert2.min.css" rel="stylesheet" />

<script src="<?= $assets->baseUrl ?>/plugins/datatable/js/jquery.dataTables.min.js"></script>
<script src="<?= $assets->baseUrl ?>/plugins/datatable/js/dataTables.bootstrap5.min.js"></script>
<script src="<?= $assets->baseUrl ?>/plugins/sweetalert2/js/sweetalert2.all.min.js"></script>
<script src="<?= $assets->baseUrl ?>/js/component-popovers-tooltips.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.3.200/pdf.js"></script>
<!--start breadcrumb-->
<div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
    <div class="breadcrumb-title pe-3">Document Eksternal</div>
    <div class="ps-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb mb-0 p-0 align-items-center">
                <li class="breadcrumb-item"><a href="javascript:;"><ion-icon name="book-outline"></ion-icon></a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">List Document</li>
            </ol>
        </nav>
    </div>
    <div class="ms-auto">
        <div class="btn-group">
            <?=Html::tag('a','<i class="fadeIn animated bx bx-plus"></i> Add',['href'=>Url::to(['/document/eksternal/new']),'class'=>'btn btn-sm btn-outline-primary']) ?>
        </div>
    </div>
</div>
<!--end breadcrumb-->
<h6 class="mb-0 text-uppercase">List Document</h6>
<hr/>
<div class="card radius-10">
    <div class="card-body">
        <div class="table-responsive">
               <table id="data_table" class="table table-responsive" style="width:100%">
                <thead>
                <tr>
                   
                <th>Dokumen</th>
                    <th>Keterangan</th>
                    <th>Opsi</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" tabindex="-1" id="mdl">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>Modal body text goes here.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        const loading=  '<div align="center"><i class="fa fa-spinner fa-spin fa-pulse fa-4x fa-fw margin-bottom"></i></div>';
        let mv=$('#mdl');
        let url="<?= Url::to() ?>"
        let urlDetail="<?= Url::to(['/document/default/details']) ?>"

        let row=null
        let groupColumn = 1;
        let table=$('#data_table').DataTable({
            // "orderCellsTop": true,
            rowId:'id',
            scrollX: true,
            // "scrollY": "400px",
            processing: true,
            serverSide: true,

            // "columnDefs": [
            //     { "visible": false, "targets": groupColumn }
            // ],
            ajax:{
                url:url,
                data:function(d){
                    d.type=2;
                    return d
                }
            },
            columns:[
                {className:'details-control',
                    orderable: false,
                    data: 'detail',
                    defaultContent: '',
                    target:0,
                    width: '5%'},
                
                {className:'edits',data:'description',orderable:true},
                {data:'actions',orderable:false,width:'15%'}
                
            ],
            order: [[ 2, 'asc' ]],
            displayLength: 25,
            // "drawCallback": function ( settings ) {
            //     var api = this.api();
            //     var rows = api.rows( {page:'current'} ).nodes();
            //     var last=null;
            //     api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
            //         if ( last !== group ) {
            //             $(rows).eq( i ).before(
            //                 '<tr class="group"><td colspan="4">'+group+'</td></tr>'
            //             );
            //             last = group;
            //         }
            //     } );
            // },
        });
        // Order by the grouping
        
        $('#data_table').on( 'click', 'tr td.details-control', function () {

            let tr = $(this).closest('tr');
            tr.toggleClass('selected')
            row = table.row(tr);
            let id = row.data().id;
            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('details');
            } else {
                // Open this row
                row.child(loading);
                $.get(urlDetail,{
                    id:id,
                    _csrf:"<?= Yii::$app->request->getCsrfToken() ?>"
                },function (res){
                    row.child(res)
                })
                row.child.show();

                tr.addClass('details');
            }
        } );

        $('body').delegate('[data-action=new_document]','click',function (e){
            e.preventDefault();
            mv.modal('show');
            // let tr = $(this).closest('tr');
            // row = table.row(tr);
            // console.log(row);
            $.ajax({
                method:'GET',
                url:$(this).attr('href'),
                beforeSend:function (){
                    mv.find('.modal-body').html('<p> Please  wait</p>');
                },
                success:function (res){
                    mv.find('h5').html('Add Document Internal');
                    mv.find('.modal-footer').html('')
                    mv.find('.modal-body').html(res);
                },
                error:function (res){
                    mv.hide();
                }
            })
        })
        $('body').delegate('[data-action=edit-jenis]','click',function (e){
            e.preventDefault();
            mv.modal('show');
            let tr = $(this).closest('tr');
            row = table.row(tr);
            // console.log(row);
            // console.log(row.data());
            $.ajax({
                method:'GET',
                url:$(this).attr('href'),
                beforeSend:function (){
                    mv.find('.modal-body').html('<p> Please  wait</p>');
                },
                success:function (res){
                    mv.find('.modal-footer').html('')
                    mv.find('.modal-body').html(res);
                },
                error:function (res){
                    mv.hide();
                }
            })
        })

        $('body').delegate('[data-action=view]','click',function (e){
            e.preventDefault();
            mv.find('.modal-dialog').addClass('modal-fullscreen');
            mv.modal('show');
            $.ajax({
                method:'GET',
                url:$(this).attr('href'),
                beforeSend:function (){
                    mv.find('.modal-body').html('<p> Please  wait</p>');
                },
                success:function (res){
                    const f=res.file;
                    mv.find('h5').html('View')
                    mv.find('.modal-footer').html('<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button')
                    mv.find('.modal-body').html(res.form);
                },
                error:function (res){
                    mv.hide();
                }
            })
        })
        mv.delegate('#submit','click',function (e){
            e.preventDefault();
            let form=mv.find('form');
            let data = new FormData(form[0]);
            $.ajax({
                url: $(form).attr('action'),
                method: $(form).attr('method'),
                data: data,
                processData: false,
                contentType: false,
                beforeSend: function(e) {
                    form.find('#submit').prop('disabled',true);
                },
                error: function (response) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Tidak bisa di save',
                    })
                    // alert_notif({status: "error", message: 'tidak bisa save'});
                    mv.modal('hide');
                     form.find('#submit').prop('disabled',false);
                },
                success:function (res){
                    console.log(res.data)
                    mv.modal('hide');
                    table.row(row).data(res.data)
                    // row.data(res.data);
                }
            })
        })
        $('body').delegate('[data-action=delete]','click',function (e){
            e.preventDefault();
            let elm=$(this);
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        method:'POST',
                        url:elm.attr('href'),
                        success:function (res){
                            Swal.fire(
                                'Deleted!',
                                'Your file has been deleted.',
                                'success'
                            )
                            table.ajax.reload();
                        },
                        error:function (res,xr){
                            Swal.fire(
                                'Not Deleted!',
                                'Your file.',
                                'error'
                            )
                        }
                    })
                }
            })
        })
    })

</script>

