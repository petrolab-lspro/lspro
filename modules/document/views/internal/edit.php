<?php
/**
 * create.php
 * Created By
 * feri_
 * 23/01/2023
 */
use app\synui\ThemeAssets;
$assets=ThemeAssets::register($this);
?>
<link href="<?= $assets->baseUrl ?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?= $assets->baseUrl ?>/plugins/select2/css/select2-bootstrap4.css" rel="stylesheet" />
<script src="<?= $assets->baseUrl ?>/plugins/select2/js/select2.min.js"></script>
<div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
    <div class="breadcrumb-title pe-3">Document Internal</div>
    <div class="ps-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb mb-0 p-0 align-items-center">
                <li class="breadcrumb-item"><a href="javascript:;"><ion-icon name="book-outline"></ion-icon></a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">Edit Document</li>
            </ol>
        </nav>
    </div>
</div>
<div class="row">
    <div class="col-xl-12 mx-auto">
        <h6 class="mb-0 text-uppercase">Edit Jenis Document</h6>
        <hr/>
        <div class="card">
            <div class="card-body">
                <div class="p-4 border rounded">
                    <?= $this->render('form',['model'=>$model]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
