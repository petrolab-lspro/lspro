<?php

use yii\helpers\Html;
use yii\helpers\Url;

//app\modules\mutu\assets\MutuAssets::register($this);

// Yii::$app->clientScript->registerCoreScript('jquery.ui');
// $this->registerJsFile('@app/modules/invoice/assets/file.js');
?>


<script>
window.onload = function() {
    document.addEventListener("contextmenu", function(e){
        e.preventDefault();
        }, false);
}

$(document).contextmenu(function() { return false;});
url = "<?= $model->urlFile; ?>";
var thePdf = null;
var scale = 1.7;

$(document).ready(function () {
    pdfjsLib.getDocument(url).promise.then(function(pdf) {
        thePdf = pdf;
        viewer = document.getElementById('pdf-viewer');

        for(page = 1; page <= pdf.numPages; page++) {
            canvas = document.createElement("canvas");
            canvas.className = 'pdf-page-canvas';
            viewer.appendChild(canvas);
            renderPage(page, canvas);
        }
    });
    function renderPage(pageNumber, canvas) {
        thePdf.getPage(pageNumber).then(function(page) {
            viewport = page.getViewport(scale);
            canvas.height = viewport.height;
            canvas.width = viewport.width;
            page.render({canvasContext: canvas.getContext('2d'), viewport: viewport});
        });
    }
})


</script>
    </head>
    <body>
        <div class="left" id="pdf-viewer"> </div>
    </body>
</html>
