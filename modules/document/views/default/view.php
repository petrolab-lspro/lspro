<?php
/**
 * Feri
 * 2023-05-29
 */
use app\synui\ThemeAssets;
use yii\helpers\Url;
use yii\bootstrap5\Html;
$assets=ThemeAssets::register($this);
$req=Yii::$app->request->queryParams;
?>
<link href="<?= $assets->baseUrl ?>/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
<link href="<?= $assets->baseUrl ?>/plugins/sweetalert2/css/sweetalert2.min.css" rel="stylesheet" />
<script src="<?= $assets->baseUrl ?>/plugins/datatable/js/jquery.dataTables.min.js"></script>
<script src="<?= $assets->baseUrl ?>/plugins/datatable/js/dataTables.bootstrap5.min.js"></script>
<script src="<?= $assets->baseUrl ?>/plugins/sweetalert2/js/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.3.200/pdf.js"></script>
<div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
    <div class="breadcrumb-title pe-3">View Document <?= ' '.$req['jenis'] ?></div>
    <div class="ps-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb mb-0 p-0 align-items-center">
                <li class="breadcrumb-item"><a href="javascript:;"><ion-icon name="book-outline"></ion-icon></a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">List Document</li>
            </ol>
        </nav>
    </div>
    <div class="ms-auto">
        <div class="btn-group">            
        </div>
    </div>
</div>
<!--end breadcrumb-->
<h6 class="mb-0 text-uppercase">List Document</h6>
<hr/>
<div class="card radius-10">
    <div class="card-body">
        <div class="table-responsive">
            <table id="data_table" class="table table-responsive" style="width:100%">
                <thead>
                <tr>
                    <th>Document</th>
                    <th>Rev</th>
                    <th>Level</th>
                    <th>Tahun</th>
                    <th>Dibuat Oleh Upload</th>
                    <th>Tanggal Upload</th>
                    <th>Opsi</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" tabindex="-1" id="mdl">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>Modal body text goes here.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        let mv=$('#mdl');
        let eUrl=null;
        let row=null;
        let objName=null;
        let url="<?= Url::to() ?>"
        let token="<?=Yii::$app->request->getCsrfToken(); ?>"   
        let csrf="<?= $token=Yii::$app->request->csrfParam; ?>"
        let table=$('#data_table').DataTable({
            // "orderCellsTop": true,
            rowId:'id',
            scrollX: true,
            // "scrollY": "400px",
            processing: true,
            serverSide: true,

            // "columnDefs": [
            //     { "visible": false, "targets": groupColumn }
            // ],
            ajax:{
                url:url,
                // data:function(d){
                //     d.type=1;
                //     return d
                // }
            },
            columns:[
                // {className:'details-control',
                //     orderable: false,
                //     data: 'detail',
                //     defaultContent: '',
                //     target:0,
                //     width: '5%'},
                
                {data:'name',orderable:true},
                {data:'revText',orderable:false,width:'10%'},
                {data:'levelText',orderable:false,width:'15%'},
                {data:'tahunText',orderable:false,width:'20%'},
                {data:'user_create',orderable:false,width:'10%'},
                {data:'create_at',orderable:false,width:'10%'},
                {data:'actions',orderable:false,width:'5%'}   
            ],
            order: [[ 1, 'asc' ]],
            displayLength: 25,

        });

        $('body').delegate('[data-action=view]','click',function (e){
            e.preventDefault();
            mv.find('.modal-dialog').addClass('modal-fullscreen');
            mv.modal('show');
            $.ajax({
                method:'GET',
                url:$(this).attr('href'),
                beforeSend:function (){
                    mv.find('.modal-body').html('<p> Please  wait</p>');
                },
                success:function (res){
                    const f=res.file;
                    mv.find('h5').html('View')
                    mv.find('.modal-footer').html('<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button')
                    mv.find('.modal-body').html(res.form);
                },
                error:function (res){
                    mv.hide();
                }
            })
        })
 
        $('body').delegate('.txt','blur',function(e){
            let value=$(this).val();
           
            let form_data=new FormData();
            if(objName==='tahun'){
                form_data.append('tahun',value);
            }
            if(objName==='rev'){
                form_data.append('rev',value);
            }
            if(objName==='level'){
                form_data.append('level',value);
            }
            form_data.append(csrf,token)
            $.ajax({
                url:eUrl,
                method:'POST',
                data: form_data,
                processData: false,
                contentType: false,
            }).done(function(res){
                console.log(res)
                row.data(res.data);
            }).fail(function(err,res){
                console.log(res)
            })
            // let id=$(this).attr('data-value');
            // console.log(value,id);

            //  row.data()
            // console.log(id)
        })
        $('body').delegate('.txt','click',function(e){
            let elm=$(this);
            let tr=elm.closest('tr')
            row=table.row(tr);
            eUrl=elm.attr('data-url');
            objName=elm.attr('name');
            // console.log($(this).attr('data-url'));
        })

        $('body').delegate('[data-action=delete]','click',function (e){
            e.preventDefault();
            let elm=$(this);
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        method:'POST',
                        url:elm.attr('href'),
                        success:function (res){
                            Swal.fire(
                                'Deleted!',
                                'Your file has been deleted.',
                                'success'
                            )
                            table.ajax.reload();
                        },
                        error:function (res,xr){
                            Swal.fire(
                                'Not Deleted!',
                                'Your file.',
                                'error'
                            )
                        }
                    })
                }
            })
        })
    })
</script>