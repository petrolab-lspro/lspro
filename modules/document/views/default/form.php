<?php
/**
 * form.php
 * Created By
 * feri_
 * 23/01/2023
 */
use yii\helpers\Url;
use yii\bootstrap5\ActiveForm;
$csrf=Yii::$app->request->csrfParam;
$token= Yii::$app->request->getCsrfToken();
?>
<?php $form = ActiveForm::begin([
    'id' => 'document-form',
//        'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{input}\n{error}",
        'labelOptions' => ['class' => 'form-label'],
        'inputOptions' => ['class' => 'form-control'],
//        'errorOptions' => ['class' => 'col-lg-7 invalid-feedback'],
    ],
    'options' => [
        'class'=>'row g-3',
        'enctype' => 'multipart/form-data'
    ]
]); ?>
    <div class="col-md-3">
        <?= $form->field($model,'category_id')->dropDownList($model->category_id==null?[]:[$model->category_id=>$model->description],[
            'data-url'=>Url::to(['/document/default/list-category','type'=>1]),'class'=>'select2'])->label('Jenis') ?>
        <?= $form->field($model,'name')->hiddenInput()->label(false) ?>
    </div>
    <div class="col-md-2">
        <?= $form->field($model,'level')->textInput(['type'=>'number','value'=>1]) ?>
    </div>
    <div class="col-md-2">
        <?= $form->field($model,'rev')->textInput(['type'=>'number','value'=>0])->label('Revisi') ?>
    </div>

    <div class="col-md-2">
        <?= $form->field($model,'tahun')->textInput() ?>
    </div>


    <div class="col-md-12">
        <?= $form->field($model,'fileUpload')->fileInput() ?>
    </div>

    <div class="col-12">
        <button class="btn btn-primary" type="submit">Submit</button>
    </div>
<?php ActiveForm::end(); ?>