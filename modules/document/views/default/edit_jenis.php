<?php
use yii\bootstrap5\ActiveForm;
use app\synui\ThemeAssets;
$assets = ThemeAssets::register($this);
?>
<link href="<?= $assets->baseUrl ?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<script src="<?= $assets->baseUrl ?>/plugins/select2/js/select2.min.js"></script>



<?php 
    
    $form = ActiveForm::begin([
    'id' => 'form-jenis',
//        'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{input}\n{error}",
        'labelOptions' => ['class' => 'form-label'],
        'inputOptions' => ['class' => 'form-control'],
//        'errorOptions' => ['class' => 'col-lg-7 invalid-feedback'],
    ],
    'options' => [
        'class'=>'row g-3',
    ]
]); ?>

<div class="col-md-12">
    <?= $form->field($model,'description')->textInput(['max']) ?>
</div>
<div class="col-md-12">
    <?= $form->field($model,'userArray')->dropdownList($model->listUser,['class'=>'select2ajx','multiple'=>'multiple'])->label('User') ?>
</div>
<div class="col-md-12">
    <button type="submit" id="submit" class="btn btn-primary">Submit</button>
</div>
<?php ActiveForm::end(); ?> 
<script>
    $(function(){
        $('.select2ajx').select2();
    })
</script>

