<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the models class for table "user".
 *
 * @property int $id_user
 * @property string|null $username
 * @property string|null $access_token
 * @property string|null $password_hash
 * @property string|null $auth_key
 * @property string|null $create_at
 * @property string|null $update_at
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['create_at', 'update_at'], 'safe'],
            [['username'], 'string', 'max' => 50],
            [['level'],'integer'],
            [['access_token', 'password_hash', 'auth_key'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_user' => 'Id Users',
            'username' => 'Username',
            'access_token' => 'Access Token',
            'password_hash' => 'Password Hash',
            'auth_key' => 'Auth Key',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
        ];
    }
}
