<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the models class for table "tbl_category_document".
 *
 * @property int $id
 * @property string $description
 *
 * @property TblDocument[] $tblDocuments
 */
class TblCategoryDocument extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_category_document';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'required'],
            [['type','level'], 'integer'],
            [['description'], 'string', 'max' => 200],
            [['user_id'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
            'type'=>'Type',
            'level'=>'Level'
        ];
    }

    /**
     * Gets query for [[TblDocuments]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTblDocuments()
    {
        return $this->hasMany(TblDocument::class, ['category_id' => 'id']);
    }
}
