<?php

namespace app\modelsDB;

use Yii;

/**
 * This is the models class for table "tbl_document".
 *
 * @property int $id
 * @property int|null $category_id
 * @property string $name
 * @property string $file_name
 * @property int|null $rev
 * @property int|null $level
 * @property string|null $user_create
 * @property string|null $create_at
 * @property string|null $update_at
 *
 * @property TblCategoryDocument $category
 */
class
TblDocument extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_document';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'rev', 'level','type'], 'integer'],
            [['name', 'file_name'], 'required'],
            [['create_at', 'update_at'], 'safe'],
            [['name', 'file_name'], 'string', 'max' => 200],
            [['user_create','tahun'], 'string', 'max' => 45],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblCategoryDocument::class, 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'name' => 'Name',
            'file_name' => 'File Name',
            'rev' => 'Rev',
            'level' => 'Level',
            'user_create' => 'User Create',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
        ];
    }

    /**
     * Gets query for [[Category]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(TblCategoryDocument::class, ['id' => 'category_id']);
    }
}
