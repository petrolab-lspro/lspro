<?php
/**
 * index.php
 * Created By
 * feri_
 * 11/01/2023
 */
$this->title='About';
?>
<div class="card radius-10">
    <div class="card-body">
        <h4>Welcome to LSPro PT Petrolab Services</h4>
        <p>Member of PT. Petrolab Services.</p>
        <p>Lembaga Sertifikasi Produk (LSPro) PT. Petrolab Services adalah lembaga sertifikasi dibawah naungan PT. Petrolab Services sebagai one stop services dibidang sertifikasi mutu dan uji mutu Pelumas  yang sesuai dengan Standar Nasional Indonesia (SNI) yang berlaku.
</p>
<p>Lembaga Sertifikasi Produk PT. Petrolab Services memiliki sumber daya Auditor dan PPC yang tersertifikasi serta kompeten dibidangnya. Lembaga Sertifikasi Produk PT. Petrolab Services didukung oleh Laboratorium dengan peralatan dan teknologi terbaru yang memiliki kecepatan dan ketepatan uji.</p>
    </div>
</div>
<div class="card radius-10 w-100">


