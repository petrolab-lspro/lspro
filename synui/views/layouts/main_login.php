<?php
/**
 * main_login.php
 * Created By
 * feri_
 * 11/01/2023
 */
use app\synui\ThemeAssets;
use yii\bootstrap5\Html;
if($this->title==''){
    $this->title==Yii::$app->name;
}

//meta
$this->registerMetaTag(['name' => 'description', 'content' => $this->title]);
//$this->registerMetaTag(['charset' => Yii::$app->charset]);
//$this->registerMetaTag(['http-equiv' => 'X-UA-Compatible', 'content' => 'IE=edge,chrome=1']);
$this->registerMetaTag(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no, minimal-ui']);
$this->registerMetaTag(['name' => 'apple-mobile-web-app-capable', 'content' => 'yes']);
$this->registerMetaTag(['name' => 'msapplication-tap-highlight', 'content' => 'no']);
$this->registerMetaTag(['name' => 'author', 'content' => 'Petrolab IT Team']);
$assets=ThemeAssets::register($this);
?>
<!doctype html>
<html lang="en" class="light-theme">

<head>
    <!-- Required meta tags -->
<!--    <meta charset="utf-8">-->
<!--    <meta name="viewport" content="width=device-width, initial-scale=1">-->
    <?= Html::csrfMetaTags() ?>
    <!-- Required meta tags -->
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- loader-->
    <link href="<?= $assets->baseUrl ?>/css/pace.min.css" rel="stylesheet" />
    <script src="<?= $assets->baseUrl ?>/js/pace.min.js"></script>

    <!--plugins-->
    <link href="<?= $assets->baseUrl ?>/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" />

    <!-- CSS Files -->
    <link href="<?= $assets->baseUrl ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= $assets->baseUrl ?>/css/bootstrap-extended.css" rel="stylesheet">
    <link href="<?= $assets->baseUrl ?>/css/style.css" rel="stylesheet">
    <link href="<?= $assets->baseUrl ?>/css/icons.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">

    <title><?= Html::encode($this->title) ?></title>
</head>

<body class="bg-white">

<!--start wrapper-->
<div class="wrapper">
    <div class="">
        <div class="row g-0 m-0">
            <div class="col-xl-6 col-lg-12">
                <div class="login-cover-wrapper">
                    <div class="card shadow-none">
                        <div class="card-body">
                            <div class="text-center">
                                <h4>Sign In</h4>
                                <p>Sign In to your account</p>
                            </div>
                                <?= $content ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-12">
                <div class="position-absolute top-0 h-100 d-xl-block d-none login-cover-img">
                    <div class="text-white p-5 w-100">

                    </div>
                </div>
            </div>
        </div>
        <!--end row-->
    </div>
</div>
<!--end wrapper-->


</body>

</html>