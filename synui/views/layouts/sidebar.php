<?php
use yii\helpers\Url;
$param=Yii::$app->request->queryParams;
?>
<ul class="metismenu" id="menu">
    <?php if (Yii::$app->user->isGuest){?>
    <a href="<?= Url::to(['/site/login']) ?>" class="has-arrow">
        <div class="parent-icon"><ion-icon name="home-sharp"></ion-icon>
        </div>
        <div class="menu-title">Login</div>
    </a>
    <?php }else { ?>
<!--    <li>-->
<!--        <a href="javascript:;" class="has-arrow">-->
<!--            <div class="parent-icon"><ion-icon name="home-sharp"></ion-icon>-->
<!--            </div>-->
<!--            <div class="menu-title">Dashboard</div>-->
<!--        </a>-->
<!--        <ul>-->
<!--            <li> <a href="index.html"><ion-icon name="ellipse-outline"></ion-icon>Default</a>-->
<!--            </li>-->
<!--            <li> <a href="index2.html"><ion-icon name="ellipse-outline"></ion-icon>Analytics</a>-->
<!--            </li>-->
<!--            <li> <a href="index3.html"><ion-icon name="ellipse-outline"></ion-icon>eCommerce</a>-->
<!--            </li>-->
<!--        </ul>-->
<!--    </li>-->
        <li class="<?= in_array(Yii::$app->controller->getRoute(),['document/default'])?'mm-active':''?>">
            <a href="javascript:;"  class="has-arrow">
                <div class="parent-icon"><ion-icon name="book-sharp"></ion-icon>
                </div>
                <div class="menu-title">Document LSPro</div>
            </a>
            <ul>
            <li class="<?= isset($param['jenis'])&& $param['jenis']=='ISO 17065'?'mm-active':''?>"> <a href="<?= Url::to(['/document/default','jenis'=>'ISO 17065']) ?>"><ion-icon name="ellipse-outline"></ion-icon>ISO 17065</a>
                </li>
                <li class="<?= isset($param['jenis'])&& $param['jenis']=='internal'?'mm-active':''?>"> <a href="<?= Url::to(['/document/default','jenis'=>'internal']) ?>"><ion-icon name="ellipse-outline"></ion-icon>Internal</a>
                </li>
                <li class="<?= isset($param['jenis'])&& $param['jenis']=='eksternal'?'mm-active':''?>"> <a href="<?= Url::to(['/document/default','jenis'=>'eksternal']) ?>"><ion-icon name="ellipse-outline"></ion-icon>External</a>
                </li>
            </ul>
        </li>
        <?php if(Yii::$app->user->identity->level==2){?>
            <li class="<?= in_array(Yii::$app->controller->getRoute(),['user/default','user/default/create','user/default/edit'])?'mm-active':'' ?>">
                <a href="<?= Url::to(['/user/default']) ?>">
                    <div class="parent-icon"><ion-icon name="person-circle-sharp"></ion-icon>
                    </div>
                    <div class="menu-title">User</div>
                </a>
            </li>
            <?php }?>
    <?php } ?>
</ul>