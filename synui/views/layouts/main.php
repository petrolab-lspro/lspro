<?php
use app\synui\ThemeAssets;
use yii\bootstrap5\Html;
use yii\helpers\Url;
$asset = ThemeAssets::register($this);
$this->registerMetaTag(['name' => 'description', 'content' => $this->title]);
$this->registerMetaTag(['charset' => Yii::$app->charset]);
$this->registerMetaTag(['http-equiv' => 'X-UA-Compatible', 'content' => 'IE=edge,chrome=1']);
//$this->registerMetaTag(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no, minimal-ui']);
$this->registerMetaTag(['name' => 'apple-mobile-web-app-capable', 'content' => 'yes']);
$this->registerMetaTag(['name' => 'msapplication-tap-highlight', 'content' => 'no']);
$this->registerMetaTag(['name' => 'author', 'content' => 'Petrolab IT Team']);

?>
<!doctype html>
<html lang="<?= Yii::$app->language ?>" class="light-theme">
<head>
    <?= Html::csrfMetaTags() ?>
    <!-- Required meta tags -->
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- loader-->
    <link href="<?= $asset->baseUrl ?>/css/pace.min.css" rel="stylesheet" />
    <script src="<?= $asset->baseUrl ?>/js/pace.min.js"></script>

    <!--plugins-->
    <link href="<?= $asset->baseUrl ?>/plugins/simplebar/css/simplebar.css" rel="stylesheet" />
    <link href="<?= $asset->baseUrl ?>/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" />
    <link href="<?= $asset->baseUrl ?>/plugins/metismenu/css/metisMenu.min.css" rel="stylesheet" />

    <!-- CSS Files -->
    <link href="<?= $asset->baseUrl ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= $asset->baseUrl ?>/css/bootstrap-extended.css" rel="stylesheet">
    <link href="<?= $asset->baseUrl ?>/css/style.css" rel="stylesheet">
    <link href="<?= $asset->baseUrl ?>/css/icons.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">

    <!--Theme Styles-->
    <link href="<?= $asset->baseUrl ?>/css/dark-theme.css" rel="stylesheet" />
    <link href="<?= $asset->baseUrl ?>/css/semi-dark.css" rel="stylesheet" />
    <link href="<?= $asset->baseUrl ?>/css/header-colors.css" rel="stylesheet" />

    <!-- JS Files-->
    <script src="<?= $asset->baseUrl ?>/js/jquery.min.js"></script>
    <script src="<?= $asset->baseUrl ?>/plugins/simplebar/js/simplebar.min.js"></script>
    <script src="<?= $asset->baseUrl ?>/plugins/metismenu/js/metisMenu.min.js"></script>
    <script src="<?= $asset->baseUrl ?>/js/bootstrap.bundle.min.js"></script>
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
    <!--plugins-->
    <script src="<?= $asset->baseUrl ?>/plugins/perfect-scrollbar/js/perfect-scrollbar.js"></script>
    <script src="<?= $asset->baseUrl ?>/plugins/chartjs/chart.min.js"></script>
    <!-- Main JS-->
    <script src="<?= $asset->baseUrl ?>/js/main.js"></script>
    <title><?= Html::encode(Yii::$app->name) ?> | <?= $this->title ?></title>
</head>
<body>


<!--start wrapper-->
<div class="wrapper">

    <!--start sidebar -->
    <aside class="sidebar-wrapper" data-simplebar="true">
        <div class="sidebar-header">
            <div>
                <img src="<?= $asset->baseUrl ?>/images/logo_c.png"  alt="logo icon">
            </div>
            
            <div class="toggle-icon ms-auto"><ion-icon name="menu-sharp"></ion-icon>
            </div>
        </div>
        <!--navigation-->
            <?= $this->render('sidebar') ?>
        <!--end navigation-->
    </aside>
    <!--end sidebar -->

    <!--start top header-->
        <?= $this->render('head',['asset'=>$asset]) ?>
    <!--end top header-->


    <!-- start page content wrapper-->
    <div class="page-content-wrapper">
        <!-- start page content-->
        <div class="page-content">
            <?= $content ?>
        </div>
        <!-- end page content-->
    </div>
    <!--end page content wrapper-->


    <!--start footer-->
    <footer class="footer">
        <div class="footer-text">
            Copyright <?= date('Y')?> © Petrolab Services &nbsp  Develop by IT Division.
        </div>
    </footer>
    <!--end footer-->


    <!--Start Back To Top Button-->
    <a href="javaScript:;" class="back-to-top"><ion-icon name="arrow-up-outline"></ion-icon></a>
    <!--End Back To Top Button-->

    <!--start switcher-->
    <div class="switcher-body">
        <button class="btn btn-primary btn-switcher shadow-sm" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasScrolling" aria-controls="offcanvasScrolling"><ion-icon name="color-palette-sharp" class="me-0"></ion-icon></button>
        <div class="offcanvas offcanvas-end shadow border-start-0 p-2" data-bs-scroll="true" data-bs-backdrop="false" tabindex="-1" id="offcanvasScrolling">
            <div class="offcanvas-header border-bottom">
                <h5 class="offcanvas-title" id="offcanvasScrollingLabel">Theme Customizer</h5>
                <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas"></button>
            </div>
            <div class="offcanvas-body">
                <h6 class="mb-0">Theme Variation</h6>
                <hr>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="LightTheme" value="option1" checked>
                    <label class="form-check-label" for="LightTheme">Light</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="DarkTheme" value="option2">
                    <label class="form-check-label" for="DarkTheme">Dark</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="SemiDark" value="option3">
                    <label class="form-check-label" for="SemiDark">Semi Dark</label>
                </div>
                <hr/>
                <h6 class="mb-0">Header Colors</h6>
                <hr/>
                <div class="header-colors-indigators">
                    <div class="row row-cols-auto g-3">
                        <div class="col">
                            <div class="indigator headercolor1" id="headercolor1"></div>
                        </div>
                        <div class="col">
                            <div class="indigator headercolor2" id="headercolor2"></div>
                        </div>
                        <div class="col">
                            <div class="indigator headercolor3" id="headercolor3"></div>
                        </div>
                        <div class="col">
                            <div class="indigator headercolor4" id="headercolor4"></div>
                        </div>
                        <div class="col">
                            <div class="indigator headercolor5" id="headercolor5"></div>
                        </div>
                        <div class="col">
                            <div class="indigator headercolor6" id="headercolor6"></div>
                        </div>
                        <div class="col">
                            <div class="indigator headercolor7" id="headercolor7"></div>
                        </div>
                        <div class="col">
                            <div class="indigator headercolor8" id="headercolor8"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!--end switcher-->


    <!--start overlay-->
    <div class="overlay nav-toggle-icon"></div>
    <!--end overlay-->

</div>
<!--end wrapper-->
</body>
</html>