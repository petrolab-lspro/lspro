<?php
/**
 * ThemeAssets.php
 * Created By
 * feri_
 * 18/01/2023
 */

namespace app\synui;

class ThemeAssets extends \yii\web\AssetBundle
{
    public $sourcePath='@app/synui/assets';
//    public $css = [
//        'css/pace.min.css',
//        'css/bootstrap.css',
//        'css/bootstrap-extended.css',
//        'css/style.css',
//        'css/icons.css',
//        'https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap',
//        'plugins/simplebar/css/simplebar.css',
//        'plugins/perfect-scrollbar/css/perfect-scrollbar.css',
//        'plugins/metismenu/css/metisMenu.min.css',
//    ];
//
//    public $cssOptions = [
//        'media' => 'screen, print',
//        'rel' => 'stylesheet',
//
//    ];
//
//
//    public $js = [
//        'js/pace.min.js',
//        'js/jquery.min.js',
//        'plugins/simplebar/js/simplebar.min.js',
//        'plugins/metismenu/js/metisMenu.min.js',
//        'js/bootstrap.bundle.min.js',
//        ['https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js','type'=>'module'],
//        'plugins/perfect-scrollbar/js/perfect-scrollbar.js',
//
//    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap5\BootstrapAsset',
    ];

    public $publishOptions = [
        'only' => [
            'js/*',
            'css/*',
            'img/*',
        ]
    ];
}