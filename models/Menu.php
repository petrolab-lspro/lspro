<?php
/**
 * Created by
 * Users     : Wisard17
 * Date     : 10/06/2022
 * Time     : 03:35 PM
 * File Name: Menu.php
 **/

namespace app\models;

use app\hopeui\SideBar;
use Yii;
use yii\base\Model;

/**
 * Class Menu
 * @package app\models
 */
class Menu extends Model
{

    public static function listMenu()
    {
        $self = new self();
        return $self->listMenu;
    }

    /**
     * @return $this|array
     */
    public function getListMenu()
    {
//        if (Yii::$app->user->isGuest)
//            return [];
//        $user = Yii::$app->user->identity;

        return [
            'options' => ['class' => 'metismenu', 'id' => 'menu'],
            'items' => [
                ['label' => 'Login', 'icon' => 'fa fa-user', 'url' => ['/site/login'], 'visible' => Yii::$app->user->isGuest],
                ['label' => 'Dashboard', 'icon' => 'far fa-tachometer', 'visible' => !Yii::$app->user->isGuest,
                    'items' => [
                        ['label' => 'Home', 'url' => ['/site/index'],
                            'visible' => !Yii::$app->user->isGuest],
                    ],
                ],
                ['label' => 'List Topic', 'icon' => 'fa fa-user', 'url' => ['/admin/topic-training'], 'visible' => !Yii::$app->user->isGuest],
                ['label' => 'Certificate', 'icon' => 'fa fa-user', 'url' => ['/admin/cert'], 'visible' => !Yii::$app->user->isGuest],
            ],

        ];
    }
}